function loadComments() {
    $.ajax({
        type: "GET",
        url: '/comments',
        dataType: 'json',
        success: function (response) {
            $.each(response, function(i) {
                $('<tr>').html(
                    "<td colspan='3'>" + response[i].date + "</td><td>" + response[i].text + "</td>")
                    .appendTo($('#content'));
            });
        }
    });
}

function saveComment() {
    var date = getDate();
    var text = $('#text').val();
    var json = {"date":date, "text":text};
    var jsonString = JSON.stringify(json);

    $.ajax ({
        type: "POST",
        url : '/comments',
        contentType: "application/json; charset=utf-8",
        data : jsonString,
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Accept", "application/json");
            xhr.setRequestHeader("Content-Type", "application/json");
        }
    });
}

function getDate() {
    var fullDate = new Date();
    var twoDigitMonth = fullDate.getMonth()+1+"";if(twoDigitMonth.length==1) twoDigitMonth="0"+twoDigitMonth;
    var twoDigitDate = fullDate.getDate()+"";if(twoDigitDate.length==1)	twoDigitDate="0"+twoDigitDate;
    var twoDigitHours = fullDate.getHours()+"";if(twoDigitHours.length==1) twoDigitHours="0"+twoDigitHours;
    var twoDigitMinutes = fullDate.getMinutes()+"";if(twoDigitMinutes.length==1) twoDigitMinutes="0"+twoDigitMinutes;
    var twoDigitSeconds = fullDate.getSeconds()+"";if(twoDigitSeconds.length==1) twoDigitSeconds="0"+twoDigitSeconds;

    return twoDigitDate + "-" + twoDigitMonth + "-" + fullDate.getFullYear() + " "
        + twoDigitHours + ":" + twoDigitMinutes + ":" + twoDigitSeconds;
}