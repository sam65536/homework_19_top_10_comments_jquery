package com.geekhub.comments;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;

public class Comment {

    @JsonFormat(pattern="dd-MM-yyyy HH:mm:ss")
    private LocalDateTime date;
    private String text;

    public Comment() {
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
